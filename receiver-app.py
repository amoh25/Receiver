import connexion
from connexion import NoContent
import datetime
import json
import logging
import logging.config
import pykafka
from pykafka import KafkaClient
import requests
import uuid
import yaml 

def process_event(event, endpoint):
    trace_id = str(uuid.uuid4())
    event['trace_id'] = trace_id

    logger.info(f'Received {endpoint} event with trace id {trace_id}')
    client = KafkaClient(hosts=f"{conf['events']['hostname']}:{conf['events']['port']}")
    topic = client.topics[conf['events']['topic']]
    producer = topic.get_sync_producer()
    event_dict = {
        'type': endpoint,
        'datetime': datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%SZ"),
        'payload': event
    }
    json_string = json.dumps(event_dict)
    string = json_string.encode('utf-8')
    producer.produce(string)
    logger.info(f"PRODUCER::producing {event} event")
    logger.info(json_string)

    return NoContent, 201


def buy(body):
    process_event(body, 'buy')
    return NoContent, 201

def sell(body):
    process_event(body, 'sell')
    return NoContent, 201

app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("openapi.yaml", base_path="/receiver" ,strict_validation=True, validate_responses=True)

with open('receiver-app_conf.yml', 'r') as f:
    conf = yaml.safe_load(f.read())

with open('app_conf.yml', 'r') as f:
    app_config = yaml.safe_load(f.read())

with open('log_conf.yml', 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basic')
logger.info('ASSIGNMENT 3')

if __name__ == "__main__":
    app.run(port=8081)
